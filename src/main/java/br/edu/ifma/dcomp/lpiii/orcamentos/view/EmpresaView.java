package br.edu.ifma.dcomp.lpiii.orcamentos.view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class EmpresaView extends Application {

    private static Stage stage;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("empresa.fxml"));
        primaryStage.setTitle("Empresa");
        primaryStage.setScene(new Scene(root, 1280, 720));
        primaryStage.show();
        stage = primaryStage;
    }

    public static Stage stage() {
        return stage;
    }

}
