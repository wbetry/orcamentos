package br.edu.ifma.dcomp.lpiii.orcamentos.repository;

import br.edu.ifma.dcomp.lpiii.orcamentos.model.Cliente;
import br.edu.ifma.dcomp.lpiii.orcamentos.model.Empresa;

import javax.persistence.EntityManager;

public class EmpresaRepository {

    private EntityManager manager;
    private GenericRepository<Empresa> genericRepository;

    public EmpresaRepository(EntityManager manager) {
        this.manager = manager;
        this.genericRepository = new GenericRepository<>(this.manager );
    }

    public void salva(Empresa empresa) {
        this.genericRepository.salva(empresa );
    }

    public Empresa buscaPor(Integer id) {
        return this.genericRepository.buscaPorId(Empresa.class, id);
    }

}
