package br.edu.ifma.dcomp.lpiii.orcamentos.infra;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EMFactory {

    final static private EntityManagerFactory factory = Persistence.createEntityManagerFactory("orcamentos");
    private static EntityManager entityManager;

    public static EntityManager getEntityManager() {
        if (entityManager == null) {
            entityManager = factory.createEntityManager();
        }

        entityManager.close();
        entityManager = factory.createEntityManager();
        return entityManager;
    }

    public static void closeEntityManager() {
        entityManager.close();
    }

    public static void closeEntityManagerFactory() {
        factory.close();
    }

}
