package br.edu.ifma.dcomp.lpiii.orcamentos.repository;

import br.edu.ifma.dcomp.lpiii.orcamentos.model.Recurso;

import javax.persistence.EntityManager;

public class RecursoRepository {

    private EntityManager manager;
    final private GenericRepository<Recurso> genericRepository;

    public RecursoRepository(EntityManager manager) {
        this.manager = manager;
        this.genericRepository = new GenericRepository<>(this.manager );
    }

    public void salva(Recurso recurso) {
        this.genericRepository.salva(recurso );
    }

    public Recurso buscaPor(Integer id) {
        return this.genericRepository.buscaPorId(Recurso.class, id);
    }

}
