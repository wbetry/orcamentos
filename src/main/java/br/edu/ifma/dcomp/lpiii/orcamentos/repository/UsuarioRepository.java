package br.edu.ifma.dcomp.lpiii.orcamentos.repository;

import br.edu.ifma.dcomp.lpiii.orcamentos.model.Usuario;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class UsuarioRepository {

    private EntityManager manager;
    final private GenericRepository<Usuario> genericRepository;

    public UsuarioRepository(EntityManager manager) {
        this.manager = manager;
        this.genericRepository = new GenericRepository<>(this.manager );
    }

    public void salva(Usuario usuario) {
        this.genericRepository.salva(usuario );
    }

    public Usuario buscaPor(Integer id) {
        return this.genericRepository.buscaPorId(Usuario.class, id);
    }

    public Usuario buscarPor(String nick, String senha) {
        Query query = manager.createQuery("from Usuario where nick = :nick and senha = :senha");
        query.setParameter("nick", nick);
        query.setParameter("senha", senha);

        final List<Usuario> usuario = query.getResultList();

        if (usuario == null ) {
            throw new RuntimeException("Usuário ou Senha Inválidos");
        }

        return usuario.get(0);
    }

}
