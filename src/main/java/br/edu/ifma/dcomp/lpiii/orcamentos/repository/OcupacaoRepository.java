package br.edu.ifma.dcomp.lpiii.orcamentos.repository;

import br.edu.ifma.dcomp.lpiii.orcamentos.model.Ocupacao;

import javax.persistence.EntityManager;

public class OcupacaoRepository {

    private EntityManager manager;
    final private GenericRepository<Ocupacao> genericRepository;

    public OcupacaoRepository(EntityManager manager) {
        this.manager = manager;
        this.genericRepository = new GenericRepository<>(this.manager );
    }

    public void salva(Ocupacao ocupacao) {
        this.genericRepository.salva(ocupacao );
    }

    public Ocupacao buscaPor(Integer id) {
        return this.genericRepository.buscaPorId(Ocupacao.class, id);
    }

}
