package br.edu.ifma.dcomp.lpiii.orcamentos.repository;

import br.edu.ifma.dcomp.lpiii.orcamentos.model.Cliente;

import javax.persistence.EntityManager;

public class ClienteRepository {

    private EntityManager manager;
    private GenericRepository<Cliente> genericRepository;

    public ClienteRepository(EntityManager manager) {
        this.manager = manager;
        this.genericRepository = new GenericRepository<>(this.manager );
    }

    public void salva(Cliente cliente) {
        this.genericRepository.salva(cliente );
    }

    public Cliente buscaPor(Integer id) {
        return this.genericRepository.buscaPorId(Cliente.class, id );
    }

}
