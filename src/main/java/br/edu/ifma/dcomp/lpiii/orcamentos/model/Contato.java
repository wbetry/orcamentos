package br.edu.ifma.dcomp.lpiii.orcamentos.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Contato {

    @Column(name = "contato_telefone")
    private String telefone;

    @Column(name = "contato_celular")
    private String celular;

    @Column(name = "contato_email")
    private String email;

    @Column(name = "contato_site")
    private String site;

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }
}
