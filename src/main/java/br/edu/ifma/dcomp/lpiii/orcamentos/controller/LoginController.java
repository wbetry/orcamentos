package br.edu.ifma.dcomp.lpiii.orcamentos.controller;

import br.edu.ifma.dcomp.lpiii.orcamentos.infra.EMFactory;
import br.edu.ifma.dcomp.lpiii.orcamentos.model.Sessao;
import br.edu.ifma.dcomp.lpiii.orcamentos.model.Usuario;
import br.edu.ifma.dcomp.lpiii.orcamentos.repository.UsuarioRepository;
import br.edu.ifma.dcomp.lpiii.orcamentos.view.InicialView;
import br.edu.ifma.dcomp.lpiii.orcamentos.view.LoginView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    @FXML
    private TextField login_usuario;
    @FXML
    private TextField login_senha;
    @FXML

    private void loginEntrar(ActionEvent event) throws IOException {
        String usuario = login_usuario.getText();
        String senha = login_senha.getCharacters().toString();

        UsuarioRepository usuarioRepository = new UsuarioRepository(EMFactory.getEntityManager());

        Usuario login = usuarioRepository.buscarPor(usuario, senha);

        Sessao.atualiza("login", login);

        EMFactory.closeEntityManager();

        new InicialView().start(new Stage());
        LoginView.stage().close();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

}

