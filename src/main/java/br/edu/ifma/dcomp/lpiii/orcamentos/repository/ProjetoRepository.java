package br.edu.ifma.dcomp.lpiii.orcamentos.repository;

import br.edu.ifma.dcomp.lpiii.orcamentos.model.Projeto;

import javax.persistence.EntityManager;

public class ProjetoRepository {

    private EntityManager manager;
    final private GenericRepository<Projeto> genericRepository;

    public ProjetoRepository(EntityManager manager) {
        this.manager = manager;
        this.genericRepository = new GenericRepository<>(this.manager );
    }

    public void salva(Projeto projeto) {
        this.genericRepository.salva(projeto );
    }

    public Projeto buscaPor(Integer id) {
        return this.genericRepository.buscaPorId(Projeto.class, id);
    }

}
